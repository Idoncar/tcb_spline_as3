package idon.utils.interpolation {
    import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.TimerEvent;
    import flash.geom.Point;
    import flash.geom.Vector3D;
    import flash.utils.Timer;
    import flash.utils.getTimer;

    public class TCBSpline {
        private var tension:Number;
        private var bias:Number;
        private var continuity:Number;
        public var keyPoints:Vector.<KeyPoint>;
        public var minUnitDistance:Number = 1;
        private var curves:Vector.<Curve>;
        private var onMovementEndCb:Function;
        private var onMovementUpdateCb:Function;
        private var curvesCount:int;

        public var currentCurve:int = 0;
        private var moveStartTime:Number;
        private var targetDistance:Number = 0;
        private var timer:Timer;
        private var enterFrameBroadcaster:Sprite = new Sprite();
        public var isMoving:Boolean;

        /**
         * Kochanek-Bartels spline
         *
         * @param points - key points
         * @param minUnitDistance - min. unit distance. Lesser values -> Greater precision, more calculations
         * @param tension
         * @param bias
         * @param continuity
         */
        public function TCBSpline(points:Vector.<SplineKeyPoint>, minUnitDistance:Number = 1, tension:Number = 0, bias:Number = 0, continuity:Number = 0) {
            keyPoints = new <KeyPoint>[];
            this.minUnitDistance = minUnitDistance;
            var len:int = points.length, i:int = 0;
            var kp:KeyPoint, prevKp:KeyPoint;
            for (i = 0; i < len; i++) {
                kp = new KeyPoint(points[i], tension, bias, continuity);
                if (i > 0) {
                    prevKp = keyPoints[i-1];
                    kp.prev = prevKp;
                    prevKp.next = kp;
                }
                keyPoints.push(kp);
            }
            this.tension = tension;
            this.bias = bias;
            this.continuity = continuity;

            timer = new Timer(33);
            timer.addEventListener(TimerEvent.TIMER, tick, false, 0, true);
        }

        /**
         * Creates curves
         */
        public function init():void {
            curvesCount = keyPoints.length - 1;
            curves = new <Curve>[];
            for (var i:int = 0; i < curvesCount; i++)
                curves.push(new Curve(keyPoints[i], minUnitDistance));
        }

        /**
         * Starts movement
         * @param speeds - movement speed at every keyPoint (unit/sec) (See. setSpeeds method description)
         * @param onUpdatePosition - function(pos:Point):void
         * @param onComplete - function():void
         * @param updateEveryFrame - onUpdatePosition is called every frame
         * @param updateDelay - onUpdatePosition is called every updateDelay(milliseconds)
         */
        public function startMovement(onUpdatePosition:Function = null, onComplete:Function = null, updateEveryFrame:Boolean = true, updateDelay:Number = 33):void {
            stopMovement();
            isMoving = true;
            onMovementEndCb = onComplete;
            onMovementUpdateCb = onUpdatePosition;
            moveStartTime = getTimer();
            currentCurve = 0;
            curves[0].init();
            timePassedMax = curves[0].totalTime;
            distancePassed = timePassedMin = timePassed = 0;

            if (updateEveryFrame) {
                enterFrameBroadcaster.addEventListener(Event.ENTER_FRAME, tick, false, 0, true);
            } else if (updateDelay > 0) {
                timer.delay = updateDelay;
                timer.reset();
                timer.start();
            }
        }

        /**
         * Stops movement
         */
        public function stopMovement():void {
            lastDt = 0;
            if (isMoving) {
                isMoving = false;
                timer.stop();
                enterFrameBroadcaster.removeEventListener(Event.ENTER_FRAME, tick);
                var onComplete:Function = onMovementEndCb;
                onMovementEndCb = null;
                onMovementUpdateCb = null;
                if (onComplete != null) onComplete();
            }
        }

        private function tick(event:Event = null):void {
            var pos:Vector3D = currentPosition(getTimer(), moveStartTime);
            if (onMovementUpdateCb != null) {
                onMovementUpdateCb(pos);
            }
            if (lastDt == 1 && currentCurve >= curvesCount - 1) {
                stopMovement();
            }
        }

        private var lastDt:Number = 0;
        private var distancePassed:Number = 0;
        private var lastVel:Point = new Point();

        private var timePassed:Number;
        private var timePassedMax:Number;
        private var timePassedMin:Number;
        /**
         * Current position on spline.
         * @param time - Global time
         * @param moveStartTime - movement start time
         * @param speed - movement speed (unit/sec)
         * @param out
         * @return
         */
        public function currentPosition(time:Number, moveStartTime:Number, out:Vector3D = null):Vector3D {
            if (!out) out = new Vector3D();
            if (isNaN(time)) time = getTimer();
            timePassed = (time - moveStartTime) / 1000;
            var i:int, curve:Curve = curves[currentCurve];
            for (i = currentCurve; i < curvesCount - 1; i++) {
                if (timePassed >= timePassedMax) {
                    currentCurve = i + 1;
                    curve = curves[currentCurve];
                    curve.init();
                    timePassedMin = timePassedMax;
                    timePassedMax += curve.totalTime;
                    distancePassed = 0;
                    lastDt = 0;
                } else {
                    break;
                }
            }

            if (timePassed >= timePassedMax) lastDt = 1;

            timePassed -= timePassedMin; //time passed on current curve
            targetDistance = curve.v1 * timePassed + curve.acceleration * timePassed * timePassed * .5;//distance passed on current curve

            if (distancePassed <= curve.length) {
                curve = curves[currentCurve];
                while (distancePassed < targetDistance && lastDt < 1) { //finding point on curve
                    curve.getVelocity(lastDt, lastVel);
                    lastDt += minUnitDistance / lastVel.length;
                    distancePassed += minUnitDistance;
                }
                if (lastDt > 1) lastDt = 1;
                curve.getPos(lastDt, out);
            } else {
                curves[curvesCount-1].getPos(1, out);
            }
            return out;
        }

        public function get currentSpeed():Number {
            var curve:Curve = curves[currentCurve];
            return curve.v1 + (curve.v2 - curve.v1) * lastDt;
        }

        /**
         * Returns interpolated value between point set by firstPointIndex and next point
         * @param firstPointIndex - first point of a curve in points array
         * @param t -> [0,1]
         * @return
         */
        public function interpolate(firstPointIndex:int, t:Number):Vector3D {
            if (firstPointIndex >= curvesCount) return null;
            var c:Curve = curves[firstPointIndex];
            c.init();
            return c.getPos(t);
        }

        public function get currentTime():Number {
            return lastDt;
        }

        public function nextKeyPoint():SplineKeyPoint {
            return currentCurve < curves.length ? curves[currentCurve].endPoint.p : null;
        }

    }
}

import flash.geom.Vector3D;
import idon.utils.interpolation.SplineKeyPoint;

class KeyPoint extends Vector3D {
    public var prev:KeyPoint;
    public var next:KeyPoint;
    private var _ra:Point;
    private var _rb:Point;

    private var bias:Number;
    private var continuity:Number;
    private var tension:Number;
    public var p:SplineKeyPoint;

    public function KeyPoint(p:SplineKeyPoint, tension:Number = 0, bias:Number = 0, continuity:Number = 0):void {
        super(p.x, p.y, p.z);
        this.p = p;
        this.tension = tension;
        this.bias = bias;
        this.continuity = continuity;
    }

    private function initRa():Point {
        var g1:Number, g2:Number, g3:Number;
        _ra = new Point();
        if (!prev) {
            _ra.x = next.x - x;
            _ra.y = next.y - y;
            if (!next.next) { //only 2 key points case
                _ra.x *= (1 - tension);
                _ra.y *= (1 - tension);
            }
        } else if (!next) {
            _ra.x = (1.5 * (x - prev.x) - 0.5 * prev.rb.x) * (1 - tension);
            _ra.y = (1.5 * (y - prev.y) - 0.5 * prev.rb.y) * (1 - tension);
        } else {
            g1 = (x - prev.x) * (1 + bias);
            g2 = (next.x - x) * (1 - bias);
            g3 = g2-g1;
            _ra.x = (1 - tension) * (g1 + 0.5 * g3 * (1 + continuity));
            g1 = (y - prev.y) * (1 + bias);
            g2 = (next.y - y) * (1 - bias);
            g3 = g2-g1;
            _ra.y = (1 - tension) * (g1 + 0.5 * g3 * (1 + continuity));

        }
        return _ra;
    }

    private function initRb():Point {
        var g1:Number, g2:Number, g3:Number;
        _rb = new Point();
        if (!prev) {
            _rb.x = (1.5 * (next.x - x) - 0.5 * next.ra.x) * (1 - tension);
            _rb.y = (1.5 * (next.y - y) - 0.5 * next.ra.y) * (1 - tension);
        } else if (!next) {
            _rb.x = x - prev.x;
            _rb.y = y - prev.y;
            if (!prev.prev) { //only 2 key points case
                _rb.x *= (1 - tension);
                _rb.y *= (1 - tension);
            }
        } else {
            g1 = (x - prev.x) * (1 + bias);
            g2 = (next.x - x) * (1 - bias);
            g3 = g2-g1;
            _rb.x = (1 - tension) * (g1 + 0.5 * g3 * (1 - continuity));
            g1 = (y - prev.y) * (1 + bias);
            g2 = (next.y - y) * (1 - bias);
            g3 = g2-g1;
            _rb.y = (1 - tension) * (g1 + 0.5 * g3 * (1 - continuity));
        }
        return _rb;
    }

    /**
     * Starting tangent (When this point is starting point of a curve)
     */
    public function get ra():Point {
        return _ra || initRa();
    }

    /**
     * Ending tangent (When this point is an ending point of a curve)
     */
    public function get rb():Point {
        return _rb || initRb();
    }
}


import flash.geom.Point;

class Curve {
    public var startPoint:KeyPoint;
    public var endPoint:KeyPoint;

    protected var r1x:Number;
    protected var r1y:Number;
    protected var r2x:Number;
    protected var r2y:Number;
    protected var Ax:Number;
    protected var Ay:Number;
    protected var Bx:Number;
    protected var By:Number;


    protected var _length:Number = 0;
    protected var minUnitDistance:Number;

    public var v1:Number; //start speed
    public var v2:Number; //end speed
    public var totalTime:Number = 0;
    public var acceleration:Number;

    /**
     * Curve
     * @param startPoint
     * @param minUnitDistance
     */
    public function Curve(startPoint:KeyPoint, minUnitDistance:Number = 1):void {
        this.startPoint = startPoint;
        endPoint = startPoint.next;
        this.minUnitDistance = minUnitDistance;
        v1 = startPoint.p.speed;
        v2 = endPoint.p.speed;
    }

    protected var tCubed:Number;
    protected var tSquared:Number;
    public var initialized:Boolean;

    public function init():void {
        if (initialized) return;

        r1x = startPoint.ra.x;
        r1y = startPoint.ra.y;
        r2x = endPoint.rb.x;
        r2y = endPoint.rb.y;

        Ax = 6 * (startPoint.x - endPoint.x) + 3 * (r1x + r2x);
        Ay = 6 * (startPoint.y - endPoint.y) + + 3 * (r1y + r2y);
        Bx = 6 * (endPoint.x - startPoint.x) - 4 * r1x - 2 * r2x;
        By = 6 * (endPoint.y - startPoint.y) - 4 * r1y - 2 * r2y;

        initSpeeds();
        initialized = true;
    }

    /**
     * Returns position on curve at t, (t->[0,1])
     * @param t -> [0,1]
     * @param out
     * @return
     */
    public function getPos(t:Number, out:Vector3D = null):Vector3D {
        if (!out) out = new Vector3D();
        tSquared = t * t;
        tCubed = tSquared * t;
        out.x = startPoint.x * (2*tCubed - 3*tSquared + 1) + r1x * (tCubed - 2*tSquared + t) + endPoint.x * (-2*tCubed + 3*tSquared) + r2x * (tCubed - tSquared);
        out.y = startPoint.y * (2*tCubed - 3*tSquared + 1) + r1y * (tCubed - 2*tSquared + t) + endPoint.y * (-2*tCubed + 3*tSquared) + r2y * (tCubed - tSquared);
        out.z = startPoint.z + (endPoint.z - startPoint.z) * t;
        return out;
    }

    private var vOut:Point = new Point();

    /**
     * Curve derivative at t. t->[0,1]
     * @param t -> [0,1]
     * @param out
     * @return
     */
    public function getVelocity(t:Number, out:Point = null):Point {
        if (!out) out = new Point();
        out.x = t * t * Ax + t * Bx + r1x;
        out.y = t * t * Ay + t * By + r1y;
        return out;
    }

    /**
     * Sets speeds at start of the curve and at the end. Calculates acceleration and totalTime
     * @param atStart
     * @param atEnd
     * Note: If atStart == atEnd there is no acceleration. Yours truly, Captain.
     */
    public function setSpeed(atStart:Number, atEnd:Number):void {
        v1 = atStart;
        v2 = atEnd;
        totalTime = 0;
    }

    public function initSpeeds():void {
        if (totalTime > 0) return;
        totalTime = (length * 2) / (v1 + v2);
        acceleration = (v2 - v1) / totalTime;
    }

    /**
     * Curve length. (Calculated using minUnitDistance approximation)
     */
    public function get length():Number {
        if (!_length) {
            var t:Number = 0;
            while (t < 1) {
                getVelocity(t, vOut);
                t += minUnitDistance / vOut.length;
                _length += minUnitDistance;
            }
        }
        return _length;
    }
}
