package idon.utils.interpolation {
    import flash.geom.Vector3D;

    public class SplineKeyPoint extends Vector3D {
        public var speed:Number;
        public function SplineKeyPoint(x:Number = 0.0, y:Number = 0.0, z:Number = 0.0, w:Number = 0.0, speed:Number = 0.0) {
            super(x, y, z, w);
            this.speed = speed;
        }
    }
}
